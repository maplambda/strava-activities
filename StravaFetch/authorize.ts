import express from 'express';
import { AccessToken, AuthorizationCode } from "simple-oauth2";
import * as SETTINGS from "../local.settings.json"

const oAuth2Client = new AuthorizationCode({
  client: {
      id: SETTINGS.Values.ClientId,
      secret: SETTINGS.Values.Secret
  },
  auth:{
      tokenHost: 'https://www.strava.com',
      tokenPath: "/api/v3/oauth/token"
  },
  options: {
      authorizationMethod: 'body'
  }
});

const app = express();

app.get('/callback', async (req, resp) => {
  const code = req.query.code as string;
  const exchange = await exchange_authorization_code(oAuth2Client, code);
  resp.send(exchange.token);
  server.close();
});

const server = app.listen(3000, () => {
  console.log("Open the following url in your browser:");
  console.log(oAuth2Client.authorizeURL({
    redirect_uri: 'http://localhost:3000/callback',
    scope: 'activity:read_all',
    state: 'abc'
  }));
})

async function exchange_authorization_code(client: AuthorizationCode, code: string): Promise<AccessToken> {
  const token = client.getToken({
    code: code,
    redirect_uri: 'http://localhost:3000/callback',
    scope: 'activity:read_all'
  });
  return token;
}