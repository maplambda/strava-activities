import * as E from 'fp-ts/lib/Either'
import {AuthorizationCode} from "simple-oauth2";
import {AzureFunction, Context, HttpRequest} from "@azure/functions"
import {program} from "./activities"

// config

const oAuth2Client = new AuthorizationCode({
  client: {
      id: process.env["ClientId"],
      secret: process.env["Secret"]
  },
  auth:{
      tokenHost: 'https://www.strava.com',
      tokenPath: "/api/v3/oauth/token"
  },
  options: {
      authorizationMethod: 'body'
  }
});

type HttpResponse = {[key: string]: any;};

const OK = (body: any): HttpResponse => {
  return {
    status: 200,
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  }
};

export const run: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
  context.log('HTTP trigger function processed a request.');
  context.res = E.foldW(
    // return 500 on error — error info is exposed to console only
    l => {throw l}, 
    // Ok
    r => OK(r))(await program(oAuth2Client)());
};