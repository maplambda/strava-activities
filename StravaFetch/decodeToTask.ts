import * as TE from 'fp-ts/lib/TaskEither'
import * as E from 'fp-ts/lib/Either'
import * as D from 'io-ts/Decoder'

export const decodeToTask = <T>(decoder:D.Decoder<unknown, T>) => (input: unknown): TE.TaskEither<Error, T> => 
  TE.fromEither(
    E.mapLeft(x => new Error(JSON.stringify(x)))(decoder.decode(input))
);