import {pipe} from 'fp-ts/function'
import * as D from 'io-ts/Decoder'

export const Activity = D.struct({
  id: D.number
});

export type Activity = D.TypeOf<typeof Activity>;
export const Activities: D.Decoder<unknown, Array<Activity>> = D.array(Activity);

const DetailedSegmentEffort = D.struct({
  name: D.string,
  moving_time: D.number
}); 

export const BestEfforts = D.nullable(DetailedSegmentEffort);

export const DetailedActivity = pipe(
  D.struct({
    id: D.number,
    name: D.string,
    type: D.string,
    distance: D.number,
    moving_time: D.number,
    start_date_local: D.string,
    total_elevation_gain: D.number,
  }),
  D.intersect(
    D.partial({
      best_efforts: D.array(BestEfforts)
    })
  )
)
  
export type DetailedActivity = D.TypeOf<typeof DetailedActivity>;
  