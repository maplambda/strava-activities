import * as A from 'fp-ts/lib/Array'
import * as TE from 'fp-ts/lib/TaskEither'
import {pipe} from 'fp-ts/function'
import {AccessToken, AuthorizationCode} from "simple-oauth2";
import {Activity, Activities, DetailedActivity} from "./models"
import { decodeToTask } from './decodeToTask'

const getAthleteActivities = (token: AccessToken): TE.TaskEither<Error, Activity[]> => pipe(
  TE.tryCatch(
    () => fetch(`https://www.strava.com/api/v3/athlete/activities?page=1&per_page=10`, 
                    { headers: { "Authorization": "Bearer " + token.token.access_token}}).then(response => response.json()),
    l => new Error(String(l))
  ),
  TE.map((response) => response),
  TE.chain(decodeToTask(Activities))
);

const getActivity = (token: AccessToken) => (a: Activity): TE.TaskEither<Error, DetailedActivity> =>  pipe(
  TE.tryCatch(
    () => fetch(`https://www.strava.com/api/v3/activities/${a.id}`,
                    { headers: { "Authorization": "Bearer " + token.token.access_token}}).then(response => response.json()),
    l => new Error(String(l))),
  TE.map((response) => response),
  TE.chain(decodeToTask(DetailedActivity))
);

const tokenRefresh = (oAuth2Client: AuthorizationCode) => (refreshToken:string): TE.TaskEither<Error, AccessToken> => TE.tryCatch(
  () => {
    const token = oAuth2Client.createToken({refresh_token: refreshToken})
    return token.refresh()
  },
  l => new Error(String(l))
);

export const program = (oAuth2Client: AuthorizationCode) => pipe(
  TE.Do,
  TE.bind("token", x => tokenRefresh(oAuth2Client)(process.env.RefreshToken)),
  TE.chain(({token}) => pipe(
      getAthleteActivities(token),
      TE.chain(A.traverse(TE.taskEither)(getActivity(token)))
    )
  )
);