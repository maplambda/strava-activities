Returns a subset of Strava Athlete Activities + best efforts for a pre-authenticated user.

Written in a pure-functional style using [fp-ts](https://github.com/gcanti/fp-ts), [io-ts](https://github.com/gcanti/io-ts).

# Build
```npm build```
# Test
```npm test```
# Configuration
Configuration is provided by environment variables. Requires the following values of a strava registered application: ```ClientId```, ```Secret```. To obtain a refresh run ```node dist/StravaFetch/authorize.js```
## Development
### local.settings.json
```
cat << EOF > local.settings.json
{
  "IsEncrypted": false,
  "Values": {
    "FUNCTIONS_WORKER_RUNTIME": "node",
    "AzureWebJobsStorage": "",
    "ClientId": "Strava application id",
    "Secret": "Strava application secret",
    "RefreshToken": "Strava refresh token"
  },
  "Host": {
    "CORS": "*"
  }  
}
EOF
```
## Deployment
Set environment configuration via app settings.

# Run
```npm start``` will start development server.

# Notes
Authentication is provided by a stored refresh token which is exchanged for an access-token each request.